from sqlalchemy import Column, Integer, String, DateTime
from database import Base
from sqlalchemy.orm import relationship
import uuid
import datetime

class Member(Base):
	__tablename__ = 'member'
	id = Column(String(50), primary_key=True)
	account = Column(String(50), unique=True, nullable=False)
	password = Column(String(50), nullable=False)
	email = Column(String(60), unique=True, nullable=False)
	location = Column(String(100), nullable=True)
	permission = Column(String(10), nullable=True)

	def __init__(self, account, password, email, location, permission):
		self.id = str(uuid.uuid4())
		self.account = account
		self.password = password
		self.email = email
		self.location = location
		self.permission = permission

def __repr__(self):#傳遞物件
    return '<Member %r>' % (self.account)