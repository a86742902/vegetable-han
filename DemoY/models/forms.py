from flask_wtf import FlaskForm
from wtforms import Form, StringField, PasswordField, validators, IntegerField

class SigninForm(FlaskForm):
	name = StringField('輸入帳號',[validators.DataRequired()])
	password = PasswordField('輸入密碼',[validators.DataRequired()])
