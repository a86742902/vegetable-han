from flask import Flask, render_template, request, session, flash
from flask import redirect, url_for
from database import db_session, init_db
from models.member import Member
from models.forms import SigninForm
import os

app = Flask(__name__)
# 設置金鑰
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY


@app.before_request
def make_session_permanet():
    session.permanet = True


@app.before_first_request
def init():
    init_db()  # 初始化資料庫


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()  # 每次request後面就會移除資料庫的session


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/item")
def item():
    return render_template("item.html")


@app.route("/products")
def products():
    return render_template("products.html")


@app.route("/create_item")
def create_item():
    return render_template("create_item.html")


@app.route("/management")
def management():
    return render_template("management.html")


@app.route("/shopping_car")
def shopping_car():
    return render_template("shopping_car.html")


@app.route('/signin', methods=['GET', 'POST'])  # 可以同時接受兩種方法
def signin():
    # 如果用post方法 傳表單給後臺處裡 # GET 跟後端伺服器取得網頁頁面
    if request.method == 'POST':
        member = Member(
            account=request.form.get("account"),
            password=request.form.get("password"),
            email=request.form.get("email"),
            location=request.form.get("location"),
            permission=request.form.get("permission")
        )  # 接收到表單物件就可以新增一個物件
        db_session.add(member)  # 將member加入表單
        db_session.commit()  # 存入
        return redirect("/login")
    return render_template('signin.html')  # 回傳template下的signin.html


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        member = Member.query.filter(Member.account == request.form.get(
            "account")).first()
        # 詢問文件管理器找尋與表單idenitfy相符的帳號
        if member:  # 找到帳號
            if member.password == request.form.get("password"):
                session['id'] = member.id
                session['permission'] = member.permission
                return redirect('/')
            else:
                member = None  # (密碼錯誤)取消找尋此帳號
        if not member:
            error = "帳號、密碼或身分不正確"  # (無此帳號)顯示錯誤訊息
    return render_template('login.html', error=error)

# 是否是直接被呼叫的(main)) 若為True則請SERVER找出錯誤


if __name__ == '__main__':
    app.jinja_env.auto_reload = True
    app.run(
        host='127.0.0.1',
        port=5555,
        debug=True
    )
